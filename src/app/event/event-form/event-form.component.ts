import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { ShopProductService } from 'app/service/shop_product/shop-product.service';
import { ShopProductModel } from 'app/model/shop_product.model';
import { DeleteComponent } from 'app/modal/delete/delete.component';
import { AddShopProductComponent } from 'app/modal/add-shop-product/add-shop-product.component';
import { LoaderService } from 'app/service/loader/loader.service';
import { EventService } from 'app/service/event/event.service';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  event_id: number;
  edit_mode: boolean;
  event: FormGroup;
  event_image: string;
  displayedColumns: string[] = ['image', 'product_name', 'actions'];
  dataSource = new MatTableDataSource<ShopProductModel[]>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private eventService: EventService,
    private shopProductService: ShopProductService,
    @Inject('IMG_URL') private IMG_URL: string,
    public dialog: MatDialog,
    private loaderService: LoaderService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.event_id = +params.id;
      console.log(this.event_id);
      this.event = this.formBuilder.group({
        name: ['', Validators.required],
        address: ['', Validators.required],
        latitude: [''],
        longitude: [''],
      });

      if (this.event_id && this.event_id > 0) {
        this.edit_mode = true;
        this.getProfile(this.event_id);
      } else {
        this.edit_mode = false;
        this.loaderService.hide();
      }

    });
  }

  async getProfile(id) {
    await this.loaderService.show();
    let event: any = await this.eventService.getDataEvent(id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (event !== undefined) {
      if (event.status === "success") {
        this.event.controls['name'].setValue(event.data[0].name);
        this.event.controls['address'].setValue(event.data[0].address);
        console.log(event.data);
        let image: any = document.getElementById("image");
        image.src = this.checkImage(event.data[0].image);
        await this.loaderService.hide();
      }
    }
  }

  async addEvent() {
    if (this.event_image !== undefined) {
      this.event.value.image = this.event_image;
    } else {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: "กรุณาอัพโหลดรูปด้วย ",
        }
      });
      return 0;
    }
    await this.loaderService.show();
    let event: any = await this.eventService.addEvent(this.event.value).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (event !== undefined) {
      if (event.status === "success") {
        this.router.navigate(['/event/edit'], {
          queryParams: { id: event.data.id }
        });
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['success'],
          verticalPosition: 'bottom',
          data: {
            message: "เพิ่มร้านค้าเรียบร้อย ",
          }
        });
        await this.loaderService.hide();
      }
    }
  }

  async updateUser() {
    await this.loaderService.show();
    if (this.event_image !== undefined) {
      this.event.value.image = this.event_image;
    }
    console.log(this.event.value);

    let event: any = await this.eventService.updateEvent(this.event.value, this.event_id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (event !== undefined) {
      if (event.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['update'],
          verticalPosition: 'bottom',
          data: {
            message: "อัพเดตข้อมูลร้านค้า ",
          }
        });
        await this.loaderService.hide();

      }
    }
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  ClickUpload() {
    document.getElementById("clickUpload").click();
  }

  onFileChange(e) {
    var Files = e.target.files || e.dataTransfer.files;
    if (!Files.length) {
      return;
    }
    this.createImageBase64(Files[0]);
  }

  createImageBase64(files) {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      let image: any = document.getElementById("image");
      image.src = e.target.result;
      this.event_image = e.target.result;
    };
    reader.readAsDataURL(files);
  }

  ngOnInit() {
    this.getEventProduct();
  }

  async getEventProduct() {
    await this.loaderService.show();
    let shop_product: any = await this.shopProductService.getProductShop(this.event_id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (shop_product !== undefined) {
      if (shop_product.status === "success") {
        this.dataSource = new MatTableDataSource(shop_product.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        await this.loaderService.hide();
      }
    }
  }

  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  async deleteEventProduct(id) {
    await this.loaderService.show();
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '40%',
      data: {
        title: "ลบข้อมูล",
        message: " คุณต้องการจะลบข้อมูลนี้ใช่หรือไม่? ",
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        let user: any = await this.shopProductService.deleteShopProduct(result.id).catch(async (error) => {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['error'],
            verticalPosition: 'bottom',
            data: {
              message: error.statusText,
            }
          });
          await this.loaderService.hide();
        });
        if (user !== undefined) {
          if (user.status === "success") {
            this.getEventProduct();
            await this.loaderService.hide();
          }
        }
      } else {
        await this.loaderService.hide();
      }
    });
  }

  async addEventProduct() {
    await this.loaderService.show();
    const dialogRef = this.dialog.open(AddShopProductComponent, {
      width: '40%',
      data: {
        title: "เพิ่มข้อมูล",
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        let user: any = await this.shopProductService.addShopProduct({ event_id: this.event_id, product_id: result.id }).catch(async (error) => {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['error'],
            verticalPosition: 'bottom',
            data: {
              message: error.statusText,
            }
          });
          await this.loaderService.hide();
        });
        if (user !== undefined) {
          if (user.status === "success") {
            this.getEventProduct();
            await this.loaderService.hide();
          }
        }
      } else {
        await this.loaderService.hide();
      }
    });
  }



}
