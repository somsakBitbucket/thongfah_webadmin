import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { DeleteComponent } from 'app/modal/delete/delete.component';
import { LoaderService } from 'app/service/loader/loader.service';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { EventService } from 'app/service/event/event.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pathEdit = ['/event/edit'];
  displayedColumns: string[] = ['image', 'name', 'actions'];
  dataSource = new MatTableDataSource<any[]>();
  constructor(
    private eventService: EventService,
    @Inject('IMG_URL') private IMG_URL: string,
    public dialog: MatDialog,
    private loaderService: LoaderService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getShop();
  }

  async getShop() {
    await this.loaderService.show();

    let shop: any = await this.eventService.getEvent().catch((error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      this.loaderService.hide();
    });
    if (shop !== undefined) {
      if (shop.status === "success") {
        this.dataSource = new MatTableDataSource(shop.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.loaderService.hide();
      }
    }
  }

  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  async deleteShop(id) {
    await this.loaderService.show();

    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '40%',
      data: {
        title: "ลบข้อมูล",
        message: " คุณต้องการจะลบข้อมูลนี้ใช่หรือไม่? ",
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        let user: any = await this.eventService.deleteEvent(result.id).catch((error) => {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['error'],
            verticalPosition: 'bottom',
            data: {
              message: error.statusText,
            }
          });
          this.loaderService.hide();
        });
        if (user !== undefined) {
          if (user.status === "success") {
            this.getShop();
            this.loaderService.hide();
          }
        }
      } else {
        this.loaderService.hide();
      }
    });
  }


}
