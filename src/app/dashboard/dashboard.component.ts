import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { UserReportService } from 'app/service/user_report/user-report.service';
import { UserReportModel } from 'app/model/user_report.model';
import { ShopService } from 'app/service/shop/shop.service';
import { UserReportComponent } from 'app/modal/user-report/user-report.component';
import { LoaderService } from 'app/service/loader/loader.service';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild('sort_transection') sort_transection: MatSort;
  @ViewChild('sort_product') sort_product: MatSort;
  @ViewChild('sort_shop') sort_shop: MatSort;
  @ViewChild('paginator_transection') paginator_transection: MatPaginator;
  @ViewChild('paginator_product') paginator_product: MatPaginator;
  @ViewChild('paginator_shop') paginator_shop: MatPaginator;

  pathEdit = ['/user/edit'];
  displayedColumns_transection: string[] = ['date', 'amount', 'shop_name', 'user_username', 'actions'];
  displayedColumns_product: string[] = ['image', 'product_name', 'user_report_detail_quantity',];
  displayedColumns_shop: string[] = ['image', 'shop_name', 'user_report_detail_quantity',];
  dataSource_transection = new MatTableDataSource<any[]>();
  dataSource_product = new MatTableDataSource<any[]>();
  dataSource_shop = new MatTableDataSource<any[]>();
  shop: number;
  user: number;
  product: number;
  transaction: number;
  constructor(
    private userReportService: UserReportService,
    @Inject('IMG_URL') private IMG_URL: string,
    private shopService: ShopService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    this.getMonitor();
    this.getTransaction();
    this.getTopProduct();
    this.getTopShop();
  }

  async getTransaction() {
    await this.loaderService.show();
    let transaction: any = await this.userReportService.getTransactionAll().catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (transaction !== undefined) {
      if (transaction.status === "success") {
        this.dataSource_transection = new MatTableDataSource(transaction.data);
        this.dataSource_transection.sort = this.sort_transection;
        this.dataSource_transection.paginator = this.paginator_transection;
        await this.loaderService.hide();
      }
    }
  }

  async getTopProduct() {
    await this.loaderService.show();
    let product: any = await this.userReportService.getTopProduct().catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (product !== undefined) {
      if (product.status === "success") {
        this.dataSource_product = new MatTableDataSource(product.data);
        this.dataSource_product.sort = this.sort_product;
        this.dataSource_product.paginator = this.paginator_product;
        await this.loaderService.hide();
      }
      console.log("this.getProduct();", this.dataSource_product)
    }
  }

  async getTopShop() {
    await this.loaderService.show();
    let product: any = await this.userReportService.getTopShop().catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (product !== undefined) {
      if (product.status === "success") {
        this.dataSource_shop = new MatTableDataSource(product.data);
        this.dataSource_shop.sort = this.sort_shop;
        this.dataSource_shop.paginator = this.paginator_shop;
        await this.loaderService.hide();
      }
      console.log("this.getProduct();", this.dataSource_shop)
    }
  }

  async getMonitor() {
    await this.loaderService.show();
    let monitor: any = await this.shopService.getMonitor().catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (monitor !== undefined) {
      if (monitor.status === "success") {
        this.shop = monitor.data.shop;
        this.user = monitor.data.user;
        this.product = monitor.data.product;
        this.transaction = monitor.data.transaction;
        await this.loaderService.hide();
      }
    }
  }

  FilterTransection(filterValue: string) {
    this.dataSource_transection.filter = filterValue.trim().toLowerCase();
  }

  FilterProduct(filterValue: string) {
    this.dataSource_product.filter = filterValue.trim().toLowerCase();
  }

  FilterShop(filterValue: string) {
    this.dataSource_shop.filter = filterValue.trim().toLowerCase();
  }

  openDialog(data) {
    const dialogRef = this.dialog.open(UserReportComponent, {
      width: '70%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

