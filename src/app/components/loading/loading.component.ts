import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoaderService } from 'app/service/loader/loader.service';

export interface LoaderState {
  show: boolean;
}
@Component({
  selector: 'angular-loader',
  templateUrl: 'loading.component.html',
  styleUrls: ['loading.component.scss']
})
export class LoadingComponent implements OnInit {
  show = true;
  private subscription: Subscription;
  constructor(
    private loaderService: LoaderService
  ) {
  }
  ngOnInit() {
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoaderState) => {
        this.show = state.show;
        console.log(this.show);
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}