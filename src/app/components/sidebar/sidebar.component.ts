import { Component, OnInit, Inject } from '@angular/core';
import { MasterDataWebsiteService } from 'app/service/master_data_website/master-data-website.service';
import { MatSnackBar } from '@angular/material';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { LoaderService } from 'app/service/loader/loader.service';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  // { path: '/user-profile', title: 'User Profile', icon: 'person', class: '' },
  // { path: '/table-list', title: 'Table List', icon: 'content_paste', class: '' },
  { path: '/user', title: 'จัดการข้อมูลสมาชิก', icon: 'account_box', class: '' },
  { path: '/event', title: 'กิจกรรม', icon: 'event', class: '' },
  { path: '/shop', title: 'ร้านค้า', icon: 'store', class: '' },
  { path: '/product', title: 'สินค้า', icon: 'local_mall', class: '' },
  // { path: '/typography', title: 'Typography', icon: 'library_books', class: '' },
  // { path: '/icons', title: 'Icons', icon: 'bubble_chart', class: '' },
  // { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
  // { path: '/notifications', title: 'Notifications', icon: 'notifications', class: '' },
  // { path: '/login', title: 'login', icon: 'person', class: '' },
  {
    path: '/upgrade', title: 'Logout', icon: 'power_settings_new', class: 'active-pro'
  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  logo: string;
  name: string;
  constructor(
    private _snackBar: MatSnackBar,
    private masterDataWebsiteService: MasterDataWebsiteService,
    private loaderService: LoaderService,
    @Inject('IMG_URL') private IMG_URL: string,

  ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.getMasterDataWebsite();
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  logout() {
    localStorage.removeItem('userlogin');
    localStorage.removeItem('api_token');
    window.location.replace("/login");
  }

  async getMasterDataWebsite() {
    let master_data_website: any = await this.masterDataWebsiteService.getData(1)
      .catch((error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.error,
          }
        });
        this.hideLoader();
      });
    if (master_data_website !== undefined) {
      if (master_data_website.status === "success") {
        this.logo = master_data_website.data[0].image;
        this.name = master_data_website.data[0].name;
      }
    }
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }
}
