import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MatSelectSearchComponent } from './mat-select-search/mat-select-search.component';
import { MatButtonModule, MatIconModule, MatInputModule, MatProgressBarModule } from '@angular/material';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    MatSelectSearchComponent,
    LoadingComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    MatButtonModule,
    MatInputModule,
    MatSelectSearchComponent,
    LoadingComponent
  ]
})
export class ComponentsModule { }
