import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DateAdapter, MatSnackBar } from '@angular/material';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from 'app/service/user/user.service';
import * as moment from 'moment';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { LoaderService } from 'app/service/loader/loader.service';
import { MasterDataWebsiteService } from 'app/service/master_data_website/master-data-website.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  profile: FormGroup;
  change_password: FormGroup;
  master_data_website: FormGroup;
  profile_image: string;
  name: string;
  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    @Inject('IMG_URL') private IMG_URL: string,
    private dateAdapter: DateAdapter<any>,
    private _snackBar: MatSnackBar,
    private loaderService: LoaderService,
    private masterDataWebsiteService: MasterDataWebsiteService
  ) {
    this.dateAdapter.setLocale('th-TH');

    this.profile = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      birthdate: [''],
      email: ['', Validators.required],
      firstname: ['', Validators.required],
      gender_id: ['', Validators.required],
      lastname: ['', Validators.required],
      address: [''],
      age: ['', Validators.required],
    });

    this.change_password = this.formBuilder.group({
      password: ['', Validators.required],
      new_password: ['', Validators.required],
      confirm_password: ['', Validators.required],
    });

    this.master_data_website = this.formBuilder.group({
      name: ['', Validators.required],
    });


  }

  ngOnInit() {

    let user_profile = JSON.parse(localStorage.getItem('userlogin'))
    this.getProfile(user_profile.id);
    this.getMasterDataWebsite();

  }

  async getMasterDataWebsite() {
    await this.loaderService.show();
    let master_data_website: any = await this.masterDataWebsiteService.getData(1)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.error,
          }
        });
        await this.loaderService.hide();
      });
    if (master_data_website !== undefined) {
      if (master_data_website.status === "success") {
        this.profile_image = master_data_website.data[0].image;
        this.master_data_website.controls['name'].setValue(master_data_website.data[0].name);
        await this.loaderService.hide();
      }
    }
  }

  async changePassword() {
    console.log(this.change_password.value);
    let user_profile = JSON.parse(localStorage.getItem('userlogin'))

    console.log(user_profile.id);

    await this.loaderService.show();
    let change_password: any = await this.userService.changePassword(this.change_password.value, user_profile.id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.error,
        }
      });
      await this.loaderService.hide();
    });
    if (change_password !== undefined) {
      if (change_password.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['success'],
          verticalPosition: 'bottom',
          data: {
            message: "เปลี่ยนรหัสผ่านเรียบร้อย",
          }
        });
        this.change_password.reset();
        await this.loaderService.hide();
      }
    }
  }

  async getProfile(id) {
    await this.loaderService.show();
    let profile: any = await this.userService.getProfile(id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (profile !== undefined) {
      if (profile.status === "success") {
        this.profile.controls['username'].setValue(profile.data[0].username);
        this.profile.controls['age'].setValue(profile.data[0].age);
        this.profile.controls['birthdate'].setValue(profile.data[0].birthdate);
        this.profile.controls['email'].setValue(profile.data[0].email);
        this.profile.controls['firstname'].setValue(profile.data[0].firstname);
        this.profile.controls['gender_id'].setValue(profile.data[0].gender_id);
        this.profile.controls['lastname'].setValue(profile.data[0].lastname);
        this.profile.controls['address'].setValue(profile.data[0].address);
        this.name = profile.data[0].firstname + ' ' + profile.data[0].lastname
        console.log(profile.data);
        await this.loaderService.hide();
      }
    }
  }

  async updateProfile() {
    await this.loaderService.show();
    let user_profile = JSON.parse(localStorage.getItem('userlogin'))
    this.profile.controls['birthdate'].setValue(moment(this.profile.value.birthdate).format('YYYY-MM-DD'));
    let profile_image: any = await this.userService.uploadProfile(this.profile.value, user_profile.id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (profile_image !== undefined) {
      if (profile_image.status === "success") {
        console.log(profile_image.data);
        this.profile_image = profile_image.data.image;
        this.name = profile_image.data.firstname + ' ' + profile_image.data.lastname
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['update'],
          verticalPosition: 'bottom',
          data: {
            message: "อัพเดตข้อมูลเรียบร้อย ",
          }
        });
        await this.loaderService.hide();
      }
    }
  }

  async updateDataWebsite() {
    await this.loaderService.show();
    let master_data_website: any = await this.masterDataWebsiteService.updateDataWebsite(this.master_data_website.value, 1)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    if (master_data_website !== undefined) {
      if (master_data_website.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['update'],
          verticalPosition: 'bottom',
          data: {
            message: "อัพเดตข้อมูลเรียบร้อย ",
          }
        });
        setTimeout(() => {
          window.location.replace("/user-profile");
        }, 3000)
        await this.loaderService.hide();
      }
    }
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  ClickUpload() {
    document.getElementById("clickUpload").click();
  }

  onFileChange(e) {
    console.log(e);

    var Files = e.target.files || e.dataTransfer.files;
    if (!Files.length) {
      return;
    }
    this.createImageBase64(Files[0]);
  }

  createImageBase64(files) {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      this.uploadImageWebsite(e.target.result, 1);
    };
    reader.readAsDataURL(files);
  }

  async uploadImageWebsite(image, id) {
    await this.loaderService.show();
    let master_data_website: any = await this.masterDataWebsiteService.uploadImageWebsite(image, id)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    if (master_data_website !== undefined) {
      if (master_data_website.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['update'],
          verticalPosition: 'bottom',
          data: {
            message: "อัพโหลดรูปเรียบร้อย ",
          }
        });
        setTimeout(() => {
          window.location.replace("/user-profile");
        }, 3000)
        await this.loaderService.hide();
      }
    }
  }

}
