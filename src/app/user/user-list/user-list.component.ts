import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { UserService } from 'app/service/user/user.service';
import { UserModel } from 'app/model/user.model';
import { DeleteComponent } from 'app/modal/delete/delete.component';
import { LoaderService } from 'app/service/loader/loader.service';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pathEdit = ['/user/edit'];
  displayedColumns: string[] = ['image', 'username', 'shop_name', 'actions'];
  dataSource = new MatTableDataSource<UserModel[]>();

  constructor(
    private userService: UserService,
    @Inject('IMG_URL') private IMG_URL: string,
    public dialog: MatDialog,
    private loaderService: LoaderService,
    private _snackBar: MatSnackBar,

  ) { }

  ngOnInit() {
    this.getUser();
  }

  async getUser() {
    await this.loaderService.show();
    let user: any = await this.userService.getUserAll().catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (user !== undefined) {
      if (user.status === "success") {
        this.dataSource = new MatTableDataSource(user.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        await this.loaderService.hide();
      }
    }
  }

  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  checkImage(image) {
    if (image === null) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  async deleteUser(id) {
    await this.loaderService.show();
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '40%',
      data: {
        title: "ลบข้อมูล",
        message: " คุณต้องการจะลบข้อมูลนี้ใช่หรือไม่? ",
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        let user: any = await this.userService.deleteUser(result.id).catch(async (error) => {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['error'],
            verticalPosition: 'bottom',
            data: {
              message: error.statusText,
            }
          });
          await this.loaderService.hide();
        });
        if (user !== undefined) {
          if (user.status === "success") {
            this.getUser();
            await this.loaderService.hide();
          }
        }
      }
    });
  }

}
