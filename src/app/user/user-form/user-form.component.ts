import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, DateAdapter, MAT_DATE_FORMATS, MatSnackBar } from '@angular/material';
import { UserReportService } from 'app/service/user_report/user-report.service';
import { UserReportModel } from 'app/model/user_report.model';
import { UserReportComponent } from 'app/modal/user-report/user-report.component';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { ShopService } from 'app/service/shop/shop.service';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { UserService } from 'app/service/user/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserShopService } from 'app/service/user_shop/user-shop.service';
import { AlertNotiComponent } from 'app/modal/alert-noti/alert-noti.component';
import { LoaderService } from 'app/service/loader/loader.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],

})
export class UserFormComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  user: FormGroup;
  profile_image: string;
  name: any = "กรุณากรอกชื่อ";
  shop_id: number;
  pathEdit = ['/user/edit'];
  displayedColumns: string[] = ['date', 'amount', 'shop_name', 'actions'];
  dataSource = new MatTableDataSource<UserReportModel[]>();
  user_shop: any = [];
  old_user_shop_id: any;
  edit_mode: boolean
  user_id: number;
  change_password: FormGroup;
  constructor(
    private userReportService: UserReportService,
    @Inject('IMG_URL') private IMG_URL: string,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<any>,
    private shopService: ShopService,
    private _snackBar: MatSnackBar,
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userShopService: UserShopService,
    private loaderService: LoaderService
  ) {
    this.dateAdapter.setLocale('th-TH');


    this.activatedRoute.queryParams.subscribe(params => {
      this.user_id = +params.id;
      console.log(this.user_id);
      if (this.user_id && this.user_id > 0) {
        this.edit_mode = true;
        this.getTransaction(this.user_id);
        this.user = this.formBuilder.group({
          username: ['', Validators.required],
          age: [''],
          birthdate: [''],
          email: ['', Validators.required],
          firstname: ['', Validators.required],
          gender_id: [1, Validators.required],
          lastname: ['', Validators.required],
          address: [''],
        });
        this.change_password = this.formBuilder.group({
          password: ['', Validators.required],
          new_password: ['', Validators.required],
          confirm_password: ['', Validators.required],
        });
        this.getProfile(this.user_id);
      } else {
        this.getUserShop();
        this.edit_mode = false;
        this.user = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required],
          age: [''],
          birthdate: [''],
          email: ['', Validators.required],
          firstname: ['', Validators.required],
          gender_id: [1, Validators.required],
          lastname: ['', Validators.required],
          address: [''],
        });
        setTimeout(() => {
          if (this.user_shop.length == 0) {
            this.alert();
          }
        }, 1000)
      }
    });

  }

  alert() {
    const dialogRef = this.dialog.open(AlertNotiComponent, {
      width: '40%',
      data: {
        title: "แจ้งเตือน",
        message: " คุณไม่มีร้านค้าให้เลือก กรุณาไปเพิ่มร้านค้า ",
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      this.router.navigate(['/shop/add']);
    });
  }

  ngOnInit() {

  }

  async changePassword() {
    await this.loaderService.show();
    console.log(this.change_password.value);
    let user_profile = JSON.parse(localStorage.getItem('userlogin'))

    console.log(user_profile.id);

    let change_password: any = await this.userService.changePassword(this.change_password.value, this.user_id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.error,
        }
      });
      await this.loaderService.hide();
    });
    if (change_password !== undefined) {
      if (change_password.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['success'],
          verticalPosition: 'bottom',
          data: {
            message: "เปลี่ยนรหัสผ่านเรียบร้อย",
          }
        });
        this.change_password.reset();
        await this.loaderService.hide();
      }
    }
  }

  async getProfile(id) {
    await this.loaderService.show();
    let profile: any = await this.userService.getProfile(id)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    await this.getUserShop();
    if (profile !== undefined) {
      if (profile.status === "success") {
        this.user.controls['username'].setValue(profile.data[0].username);
        this.user.controls['age'].setValue(profile.data[0].age);
        this.user.controls['birthdate'].setValue(profile.data[0].birthdate);
        this.user.controls['email'].setValue(profile.data[0].email);
        this.user.controls['firstname'].setValue(profile.data[0].firstname);
        this.user.controls['gender_id'].setValue(profile.data[0].gender_id);
        this.user.controls['lastname'].setValue(profile.data[0].lastname);
        this.user.controls['address'].setValue(profile.data[0].address);
        let image: any = document.getElementById("image");
        image.src = this.checkImage(profile.data[0].image);
        this.name = profile.data[0].firstname + ' ' + profile.data[0].lastname
        this.shop_id = profile.data[0].user_shop.shop_id;
        console.log(profile.data);
        profile.data[0].user_shop.id = profile.data[0].user_shop.shop_id;
        profile.data[0].user_shop.name = profile.data[0].user_shop.shop_name;
        if (this.user_shop.length == 0) {
          this.alert();
        }
        await this.loaderService.hide();
      }
    }
  }

  async getUserShop() {
    await this.loaderService.show();
    let user_shop: any = await this.shopService.getShop()
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    if (user_shop !== undefined) {
      if (user_shop.status === "success") {
        this.user_shop = user_shop.data;
        await this.loaderService.hide();
      }
    }
  }

  async getTransaction(user_id) {
    await this.loaderService.show();
    let transaction: any = await this.userReportService.getTransactionUser(user_id)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    if (transaction !== undefined) {
      if (transaction.status === "success") {
        this.dataSource = new MatTableDataSource(transaction.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
      console.log("this.getProduct();", this.dataSource)
      await this.loaderService.hide();
    }
  }

  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(data) {
    const dialogRef = this.dialog.open(UserReportComponent, {
      width: '70%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  ClickUpload() {
    document.getElementById("clickUpload").click();
  }

  onFileChange(e) {
    var Files = e.target.files || e.dataTransfer.files;
    if (!Files.length) {
      return;
    }
    this.createImageBase64(Files[0]);
  }

  createImageBase64(files) {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      let image: any = document.getElementById("image");
      image.src = e.target.result;
      this.profile_image = e.target.result;
      if (this.edit_mode) {
        this.uploadProfileImage(e.target.result, this.user_id);
      }
    };
    reader.readAsDataURL(files);
  }

  async uploadProfileImage(image, id) {
    await this.loaderService.show();
    let profile_image: any = await this.userService.uploadProfileImage(image, id)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error,
          }
        });
        await this.loaderService.hide();
      });
    if (profile_image !== undefined) {
      if (profile_image.status === "success") {
        console.log(profile_image.data);
        this.profile_image = profile_image.data.image;
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['update'],
          verticalPosition: 'bottom',
          data: {
            message: "อัพโหลดรูปเรียบร้อย ",
          }
        });
        await this.loaderService.hide();

      }
    }
  }

  async addUser() {
    this.user.controls['birthdate'].setValue(moment(this.user.value.birthdate).format('YYYY-MM-DD'));
    if (this.shop_id !== undefined) {
      this.user.value.shop_id = this.shop_id;
    } else {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: "กรุณาเลือกร้านด้วย ",
        }
      });
      return 0;

    }
    if (this.profile_image !== undefined) {
      this.user.value.image = this.profile_image;
    } else {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: "กรุณาอัพโหลดรูปด้วย ",
        }
      });
      return 0;
    }
    this.user.value.user_type_id = 2;

    await this.loaderService.show();
    let user: any = await this.userService.addUser(this.user.value)
      .catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
    if (user !== undefined) {
      if (user.status === "success") {
        this.router.navigate([`/user`]);
        await this.loaderService.hide();
      }
    }

  }

  async updateUser() {
    this.user.controls['birthdate'].setValue(moment(this.user.value.birthdate).format('YYYY-MM-DD'));
    this.user.value.user_type_id = 2;
    if (this.shop_id !== undefined) {
      this.user.value.shop_id = this.shop_id;
    } else {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: "กรุณาเลือกร้านด้วย ",
        }
      });
      return 0;

    }
    console.log(this.user.value);

    await this.loaderService.show();
    let user: any = await this.userService.uploadProfile(this.user.value, this.user_id);
    if (user !== undefined) {
      if (user.status === "success") {
        await this.loaderService.show();
        let user_shop: any = await this.userShopService.uploadUserShop({ shop_id: this.shop_id, user_id: this.user_id }, (this.old_user_shop_id || this.shop_id))
          .catch(async (error) => {
            this._snackBar.openFromComponent(AlertComponent, {
              duration: 3000,
              panelClass: ['error'],
              verticalPosition: 'bottom',
              data: {
                message: error.statusText,
              }
            });
            await this.loaderService.hide();
          });
        if (user_shop !== undefined) {
          if (user_shop.status === "success") {
            this._snackBar.openFromComponent(AlertComponent, {
              duration: 3000,
              panelClass: ['update'],
              verticalPosition: 'bottom',
              data: {
                message: "อัพเดตข้อมูลเรียบร้อย ",
              }
            });
            this.getProfile(this.user_id)
            await this.loaderService.hide();
          }
        }
      }
    }
  }

}
