
export class UserModel {
	id?: number;
	firstname: string;
	lastname: string;
	username: string;
	age: string;
	image: string;
	birthdate: string;
	email: string;
	user_type_id: string;
	gender_id: string;

	clear() {
		this.firstname = '';
		this.lastname = '';
		this.username = '';
		this.age = '';
		this.image = '';
		this.birthdate = '';
		this.email = '';
		this.user_type_id = '';
		this.gender_id = '';
	}
}