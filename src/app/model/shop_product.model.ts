
export class ShopProductModel {
	id?: number;
	shop_id: string;
	product_id: string;
	shop_name: string;
	shop_image: string;
	shop_address: string;
	shop_latitude: string;
	shop_longitude: string;
	product_name: string;
	product_image: string;
	product_price: string;

	clear() {
		this.shop_id = '';
		this.product_id = '';
		this.shop_name = '';
		this.shop_image = '';
		this.shop_address = '';
		this.shop_latitude = '';
		this.shop_longitude = '';
		this.product_name = '';
		this.product_image = '';
		this.product_price = '';
	}
}