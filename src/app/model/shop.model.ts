
export class ShopModel {
	id?: number;
	name: string;
	image: string;
	address: string;
	latitude: string;
	longitude: string;

	clear() {
		this.name = '';
		this.image = '';
		this.address = '';
		this.latitude = '';
		this.longitude = '';
	}
}