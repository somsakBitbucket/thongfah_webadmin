
export class UserReportModel {
	id?: number;
	date: string;
	shop_name: string;
	user_username: string;
	user_report_detail: any;

	clear() {
		this.date = '';
		this.shop_name = '';
		this.user_username = '';
		this.user_report_detail = [];
	}
}