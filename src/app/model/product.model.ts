
export class ProductModel {
	id?: number;
	name: string;
	image: string;
	price: number;

	clear() {
		this.name = '';
		this.image = '';
		this.price = 0;
	}
}