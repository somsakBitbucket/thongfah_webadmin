import { Component, OnInit, Inject } from '@angular/core';
import { ProductService } from 'app/service/product/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { LoaderService } from 'app/service/loader/loader.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  product = {
    image: "",
    name: "",
    price: ""
  }

  newProduct = {
    name: "",
    price: ""
  }

  product_image: string = "";
  edit_mode: boolean;
  product_id: number;

  constructor(
    @Inject('IMG_URL') private IMG_URL: string,
    private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private loaderService: LoaderService
  ) {

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.product_id = +params.id;
      if (this.product_id && this.product_id > 0) {
        this.edit_mode = true;
        this.getProduct(this.product_id);
      } else {
        console.log('....');
        this.hideLoader();
      }
    })
  }

  async getProduct(id) {
    await this.loaderService.show();
    let product: any = await this.productService.getProductByid(id).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (product !== undefined) {
      if (product.status === "success") {
        this.product.name = product.data[0].name;
        this.product.price = product.data[0].price;
        let image: any = document.getElementById("image");
        image.src = this.checkImage(product.data[0].image);
        await this.loaderService.hide();
      }
    }
  }

  async addProduct() {
    await this.loaderService.show();
    this.product.image = this.product_image;
    let product: any = await this.productService.addProducts(this.product).catch(async (error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      await this.loaderService.hide();
    });
    if (product !== undefined) {
      if (product.status === "success") {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['success'],
          verticalPosition: 'bottom',
          data: {
            message: "เพิ่มสินค้าเรียบร้อย ",
          }
        });
        this.router.navigate(['/product']);
        await this.loaderService.hide();
      }
    }
  }

  async updateProduct() {
    await this.loaderService.show();
    if (this.product_image === "") {
      this.newProduct.name = this.product.name;
      this.newProduct.price = this.product.price;
      let product: any = await this.productService.updateProductByid(this.newProduct, this.product_id).catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
      if (product !== undefined) {
        if (product.status === "success") {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['update'],
            verticalPosition: 'bottom',
            data: {
              message: "อัพเดตข้อมูลเรียบร้อย ",
            }
          });
          this.getProduct(this.product_id);
          await this.loaderService.hide();
        }
      }
    } else if (this.product_image !== "") {
      this.product.image = this.product_image;
      let product: any = await this.productService.updateProductByid(this.product, this.product_id).catch(async (error) => {
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.statusText,
          }
        });
        await this.loaderService.hide();
      });
      if (product !== undefined) {
        if (product.status === "success") {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['update'],
            verticalPosition: 'bottom',
            data: {
              message: "อัพเดตข้อมูลเรียบร้อย ",
            }
          });
          this.getProduct(this.product_id);
          await this.loaderService.hide();
        }
      }
    }
  }

  onFileChange(e) {
    var Files = e.target.files || e.dataTransfer.files;
    if (!Files.length) {
      return;
    }
    this.getBase64(Files[0]);
  }

  getBase64(files) {
    let reader = new FileReader();
    reader.onload = (e: any) => {
      //me.modelvalue = reader.result;
      let image: any = document.getElementById("image");
      image.src = e.target.result;
      this.product_image = e.target.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
    reader.readAsDataURL(files);
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image;
    }
  }

  async hideLoader() {
    await this.loaderService.show();
    await this.loaderService.hide();
  }

}
