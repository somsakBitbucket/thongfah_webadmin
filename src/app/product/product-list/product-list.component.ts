import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { ProductService } from 'app/service/product/product.service';
import { ProductModel } from 'app/model/product.model';
import { DeleteComponent } from 'app/modal/delete/delete.component';
import { LoaderService } from 'app/service/loader/loader.service';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pathEdit = ['/product/edit'];
  displayedColumns: string[] = ['image', 'name', 'price', 'actions'];
  dataSource = new MatTableDataSource<ProductModel[]>();

  constructor(
    private productService: ProductService,
    @Inject('IMG_URL') private IMG_URL: string,
    public dialog: MatDialog,
    private loaderService: LoaderService,
    private _snackBar: MatSnackBar,


  ) { }

  ngOnInit() {
    this.getProduct();
  }

  async getProduct() {
    await this.loaderService.show();
    let product: any = await this.productService.getProduct().catch((error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      this.loaderService.hide();
    });
    if (product !== undefined) {
      if (product.status === "success") {
        this.dataSource = new MatTableDataSource(product.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.loaderService.hide();
      }
    }
  }

  async deleteProduct(id) {
    await this.loaderService.show();

    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '40%',
      data: {
        title: "ลบข้อมูล",
        message: " คุณต้องการจะลบข้อมูลนี้ใช่หรือไม่? ",
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(async result => {
      if (result !== undefined) {
        let user: any = await this.productService.deleteProductByid(result.id).catch((error) => {
          this._snackBar.openFromComponent(AlertComponent, {
            duration: 3000,
            panelClass: ['error'],
            verticalPosition: 'bottom',
            data: {
              message: error.statusText,
            }
          });
          this.loaderService.hide();
        });
        if (user !== undefined) {
          if (user.status === "success") {
            this.getProduct();
            this.loaderService.hide();
          }
        }
      } else {
        this.loaderService.hide();
      }
    });
  }

  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
