import { TestBed, inject } from '@angular/core/testing';

import { ShopProductService } from './shop-product.service';

describe('ShopProductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopProductService]
    });
  });

  it('should be created', inject([ShopProductService], (service: ShopProductService) => {
    expect(service).toBeTruthy();
  }));
});
