import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShopProductModel } from 'app/model/shop_product.model';

@Injectable({
  providedIn: 'root'
})
export class ShopProductService {
  private path: string = "shop_product"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  getProductShop(id): Promise<ShopProductModel[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/product_shop/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  addShopProduct(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/add_product`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  deleteShopProduct(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.API_URL}/delete_product/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

}
