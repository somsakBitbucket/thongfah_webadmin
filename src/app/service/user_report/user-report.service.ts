import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserReportModel } from 'app/model/user_report.model';

@Injectable({
  providedIn: 'root'
})
export class UserReportService {

  private path: string = "user_report"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  getTransactionAll(): Promise<UserReportModel[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getTransactionUser(user_id): Promise<UserReportModel[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}_admin/${user_id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getTopProduct() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}_top_product`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getTopShop() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}_top_shop`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

}
