import { TestBed, inject } from '@angular/core/testing';

import { MasterDataWebsiteService } from './master-data-website.service';

describe('MasterDataWebsiteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MasterDataWebsiteService]
    });
  });

  it('should be created', inject([MasterDataWebsiteService], (service: MasterDataWebsiteService) => {
    expect(service).toBeTruthy();
  }));
});
