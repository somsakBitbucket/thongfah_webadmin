import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoaderService } from '../loader/loader.service';

@Injectable({
  providedIn: 'root'
})
export class MasterDataWebsiteService {
  private path: string = "master_data_website"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  getData(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}`)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  updateDataWebsite(data, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/${id}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  uploadImageWebsite(image, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/image/${id}`, { image: image }, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }


}
