import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShopModel } from 'app/model/shop.model';

@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class EventService {

  private path: string = "event"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  getEvent(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getDataEvent(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getMonitor(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/monitor`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getUserEvent(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/event_user`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  addEvent(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  updateEvent(data, id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/${id}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  deleteEvent(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.API_URL}/${this.path}/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

}
