import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from 'app/model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private path: string = "user"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  login(data: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}/login`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  register(data: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}/register`, data)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getProfile(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  getUserAll(): Promise<UserModel[]> {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.API_URL}/${this.path}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  addUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.API_URL}/${this.path}/register`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  uploadProfile(data, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/${id}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  changePassword(data, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/change_password/${id}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  uploadProfileImage(image, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/profile_image/${id}`, { image: image }, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

  deleteUser(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.API_URL}/${this.path}/${id}`, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }
}
