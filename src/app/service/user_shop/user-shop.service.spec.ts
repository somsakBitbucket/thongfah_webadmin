import { TestBed, inject } from '@angular/core/testing';

import { UserShopService } from './user-shop.service';

describe('UserShopService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserShopService]
    });
  });

  it('should be created', inject([UserShopService], (service: UserShopService) => {
    expect(service).toBeTruthy();
  }));
});
