import { Injectable, Inject } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShopModel } from 'app/model/shop.model';

@Injectable({
  providedIn: 'root'
})
export class UserShopService {
  private path: string = "user_shop"

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('api_token')
    })
  }

  constructor(
    @Inject('API_URL') private API_URL: string,
    private http: HttpClient,
  ) { }

  uploadUserShop(data, id) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.API_URL}/${this.path}/updateData/${id}`, data, this.httpOptions)
        .toPromise()
        .then((res: any) => {
          (res.error) ? reject(res) : resolve(res);
        })
        .catch((error) => {
          reject(error)
        })
    });
  }

}
