import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserReportService } from 'app/service/user_report/user-report.service';
import { UserReportModel } from 'app/model/user_report.model';
import { ShopService } from 'app/service/shop/shop.service';

@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.scss']
})
export class UserReportComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pathEdit = ['/user/edit'];
  displayedColumns: string[] = ['image', 'name', 'quantity'];
  dataSource = new MatTableDataSource<UserReportModel[]>();
  shop: number;
  user: number;
  product: number;
  transaction: number;
  constructor(
    private userReportService: UserReportService,
    @Inject('IMG_URL') private IMG_URL: string,

    private shopService: ShopService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UserReportModel>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.getTransaction();
  }

  async getTransaction() {
    console.log(this.data);
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }


  Filter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
