import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertNotiComponent } from './alert-noti.component';

describe('AlertNotiComponent', () => {
  let component: AlertNotiComponent;
  let fixture: ComponentFixture<AlertNotiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertNotiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertNotiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
