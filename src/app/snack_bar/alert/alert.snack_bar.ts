import { Component, Inject } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from "@angular/material";

@Component({
  selector: 'alert.snack_bar',
  templateUrl: 'alert.snack_bar.html',

})

export class AlertComponent {
  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) {
    console.log(data);
  }
}