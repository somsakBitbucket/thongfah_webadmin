import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/service/user/user.service';
import { LoaderService } from 'app/service/loader/loader.service';
import { MatSnackBar } from '@angular/material';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { MasterDataWebsiteService } from 'app/service/master_data_website/master-data-website.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  data = {
    username: '',
    password: '',
  }
  logo: string;
  name: string;
  constructor(
    private router: Router,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private loaderService: LoaderService,
    private masterDataWebsiteService: MasterDataWebsiteService,
    @Inject('IMG_URL') private IMG_URL: string,

  ) { }

  ngOnInit() {
    this.getMasterDataWebsite();
  }

  async loginForm() {
    console.log("loginForm", this.data);
    let login: any = await this.userService.login(this.data).catch((error) => {
      this._snackBar.openFromComponent(AlertComponent, {
        duration: 3000,
        panelClass: ['error'],
        verticalPosition: 'bottom',
        data: {
          message: error.statusText,
        }
      });
      this.hideLoader();
    });
    console.log(login);
    if (login.status === "success") {
      this.keeptokenAuthen(login.data);
      this.hideLoader();
    }
    window.location.replace("/dashboard");
  }

  keeptokenAuthen(token) {
    this.clearLocalStorage();
    console.log(token);
    localStorage.setItem('api_token', token.api_token);
    delete token.api_token;
    let jsonLogin = JSON.stringify(token);
    localStorage.setItem('userlogin', jsonLogin);
    window.location.replace("/dashboard");
  }

  clearLocalStorage() {
    localStorage.removeItem('userlogin')
    localStorage.removeItem('api_token')
  }

  async getMasterDataWebsite() {
    this.showLoader();
    let master_data_website: any = await this.masterDataWebsiteService.getData(1)
      .catch((error) => {
        console.log(error);
        this._snackBar.openFromComponent(AlertComponent, {
          duration: 3000,
          panelClass: ['error'],
          verticalPosition: 'bottom',
          data: {
            message: error.error,
          }
        });
        this.hideLoader();
      });
    if (master_data_website !== undefined) {
      if (master_data_website.status === "success") {
        this.logo = master_data_website.data[0].image;
        this.name = master_data_website.data[0].name;
        this.hideLoader();

      }
    }
  }

  checkImage(image) {
    if (image === null || image === undefined) {
      return './assets/img/master-data/picture.png'
    } else {
      return this.IMG_URL + image
    }
  }

  private hideLoader(): void {
    this.loaderService.hide();
  }

  private showLoader(): void {
    this.loaderService.show();
  }

}
