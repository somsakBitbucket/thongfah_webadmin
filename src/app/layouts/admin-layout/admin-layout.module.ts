import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { LoginComponent } from '../../login/login.component';
import { RegisterComponent } from 'app/register/register.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSnackBarModule,

} from '@angular/material';
import { UserListComponent } from 'app/user/user-list/user-list.component';
import { UserFormComponent } from 'app/user/user-form/user-form.component';
import { ShopListComponent } from 'app/shop/shop-list/shop-list.component';
import { ShopFormComponent } from 'app/shop/shop-form/shop-form.component';
import { ProductFormComponent } from 'app/product/product-form/product-form.component';
import { ProductListComponent } from 'app/product/product-list/product-list.component';
import { UserReportComponent } from 'app/modal/user-report/user-report.component';
import { AlertComponent } from 'app/snack_bar/alert/alert.snack_bar';
import { DeleteComponent } from 'app/modal/delete/delete.component';
import { AddShopProductComponent } from 'app/modal/add-shop-product/add-shop-product.component';
import { ComponentsModule } from 'app/components/components.module';
import { AlertNotiComponent } from 'app/modal/alert-noti/alert-noti.component';
import { EventFormComponent } from 'app/event/event-form/event-form.component';
import { EventListComponent } from 'app/event/event-list/event-list.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    ComponentsModule,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    UserListComponent,
    UserFormComponent,
    EventFormComponent,
    EventListComponent,
    ShopListComponent,
    ShopFormComponent,
    ProductFormComponent,
    ProductListComponent,
    UserReportComponent,
    AlertComponent,
    DeleteComponent,
    AddShopProductComponent,
    AlertNotiComponent,

  ],
  entryComponents: [
    UserReportComponent,
    AlertComponent,
    DeleteComponent,
    AddShopProductComponent,
    AlertNotiComponent,
  ]
})

export class AdminLayoutModule { }
