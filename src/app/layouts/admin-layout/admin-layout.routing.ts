import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { LoginComponent } from '../../login/login.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { UserListComponent } from 'app/user/user-list/user-list.component';
import { UserFormComponent } from 'app/user/user-form/user-form.component';
import { ShopListComponent } from 'app/shop/shop-list/shop-list.component';
import { ShopFormComponent } from 'app/shop/shop-form/shop-form.component';
import { ProductListComponent } from 'app/product/product-list/product-list.component';
import { ProductFormComponent } from 'app/product/product-form/product-form.component';
import { EventListComponent } from 'app/event/event-list/event-list.component';
import { EventFormComponent } from 'app/event/event-form/event-form.component';
import { RegisterComponent } from 'app/register/register.component';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'user', component: UserListComponent },
    { path: 'user/add', component: UserFormComponent },
    { path: 'user/edit', component: UserFormComponent },
    { path: 'user/edit:id', component: UserFormComponent },
    { path: 'shop', component: ShopListComponent },
    { path: 'shop/add', component: ShopFormComponent },
    { path: 'shop/edit', component: ShopFormComponent },
    { path: 'shop/edit:id', component: ShopFormComponent },
    { path: 'event', component: EventListComponent },
    { path: 'event/add', component: EventFormComponent },
    { path: 'event/edit', component: EventFormComponent },
    { path: 'event/edit:id', component: EventFormComponent },
    { path: 'product', component: ProductListComponent },
    { path: 'product/add', component: ProductFormComponent },
    { path: 'product/edit', component: ProductFormComponent },
    { path: 'product/edit:id', component: ProductFormComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
];
